class EnablePostgresUuid < ActiveRecord::Migration
	def up
		execute 'create extension "uuid-ossp"'
	end
	def down
		execute 'drop extension "uuid-ossp"'
	end
end
