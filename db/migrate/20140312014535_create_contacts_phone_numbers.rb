class CreateContactsPhoneNumbers < ActiveRecord::Migration
  def change
  	create_table	:contacts_phone_numbers, id:  :uuid
  	add_column	:contacts_phone_numbers,	:contact_id,	:uuid
  	add_column	:contacts_phone_numbers,	:phone_number_id,	:uuid
  end
end