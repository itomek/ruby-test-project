class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses, id: :uuid do |t|
      #t.belongs_to	:contacts
      t.text				:street1
      t.text				:city

      t.timestamps
    end

    add_column	:addresses,	:contact_id,	:uuid
  end
end
