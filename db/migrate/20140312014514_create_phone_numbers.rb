class CreatePhoneNumbers < ActiveRecord::Migration
  def change
    create_table :phone_numbers, id: :uuid do |t|
      t.text :number
      t.text :name

      t.timestamps
    end
  end
end
