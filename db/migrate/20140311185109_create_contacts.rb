class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts, id: :uuid do |t|
      t.text :firstName
      t.text :lastName

      t.timestamps
    end
  end
end
