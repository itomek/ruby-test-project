class Contact < ActiveRecord::Base
	has_many	:addresses
	has_and_belongs_to_many	:phoneNumbers
end